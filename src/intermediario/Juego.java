package intermediario;

import javax.swing.JOptionPane;


public class Juego {
	//esta clase gestiona la informacion obtenida y actualizada del adivino para mostrarla en la vista

	private Adivino adivino;
	
	public Juego(){
		adivino= new Adivino();
	}
	
	public String pregunta() {
		String p= adivino.preguntar();
	
		return p;
	}
	
	public String ganador() {
		return adivino.paisAdivinado();
	}
	
	public void mostrarReglas() {	
		String reglas="Mucho";//habria que hacer que lea un archivo de texto donde explique las reglas del juego
		JOptionPane.showMessageDialog( null, reglas,
				 "REGLAS", JOptionPane.PLAIN_MESSAGE );
	}
	
	public void escucharRespuesta(String respuesta) {	
		adivino.escucharRespuesta(respuesta.equals("SI"));
	}

	public void ingresarPaisPensado() {
		// TODO Auto-generated method stub
		adivino.ingresarPaisPensado();
		
	}

	public void registrarNombre(String actionCommand) {
		// TODO Auto-generated method stub
		
	}

	public String nombreIngresado() {
		// TODO Auto-generated method stub
		return null;
	}

	public void mostrarPuntajes() {
		// TODO Auto-generated method stub
		
	}

	public void filtrarPaises(String actionCommand) {
		// TODO Auto-generated method stub
		
	}

	public void pasarPregunta() {
		// TODO Auto-generated method stub
		
	}
	
}

