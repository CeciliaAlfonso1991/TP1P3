package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import intermediario.Juego;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;

public class Menu {

	private JFrame frame;

	private Vista vista;
	private Juego met;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Menu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		vista = new Vista();
		met = new Juego();

		JButton btnJugar = new JButton("JUGAR");
		btnJugar.setForeground(new Color(238, 232, 170));
		btnJugar.setBackground(new Color(128, 128, 128));
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				vista.vistaFrame().setVisible(true);
			}
		});

		btnJugar.setBounds(517, 302, 170, 23);
		frame.getContentPane().add(btnJugar);

		JButton acercaDe = new JButton("REGLAS");
		acercaDe.setForeground(new Color(238, 232, 170));
		acercaDe.setBackground(new Color(128, 128, 128));
		acercaDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				met.mostrarReglas();
			}
		});
		acercaDe.setBounds(517, 335, 170, 23);
		frame.getContentPane().add(acercaDe);

		JButton salir = new JButton("SALIR");
		salir.setForeground(new Color(238, 232, 170));
		salir.setBackground(new Color(128, 128, 128));
		salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
			}
		});
		salir.setBounds(517, 416, 170, 23);
		frame.getContentPane().add(salir);

		JButton btnOpciones = new JButton("OPCIONES");
		btnOpciones.setForeground(new Color(238, 232, 170));
		btnOpciones.setBackground(new Color(128, 128, 128));
		btnOpciones.setBounds(517, 369, 170, 23);
		frame.getContentPane().add(btnOpciones);

		textField = new JTextField();
		textField.setBackground(new Color(240, 230, 140));
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				met.registrarNombre(arg0.getActionCommand());
			}
		});
		textField.setBounds(517, 250, 170, 23);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(Menu.class.getResource("/imagenes/titular.jpg")));
		lblNewLabel_1.setBounds(351, 39, 336, 120);
		frame.getContentPane().add(lblNewLabel_1);

		JLabel label = new JLabel("Ingrese su nombre");
		label.setToolTipText("");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(new Color(160, 82, 45));
		label.setFont(new Font("Franklin Gothic Demi", Font.BOLD, 15));
		label.setBackground(new Color(244, 164, 96));
		label.setBounds(517, 207, 170, 32);
		frame.getContentPane().add(label);

		JLabel lblIngreseSuNombre = new JLabel("Ingrese su nombre");
		lblIngreseSuNombre.setIcon(new ImageIcon(Menu.class.getResource("/imagenes/fondoJuego.jpg")));
		lblIngreseSuNombre.setToolTipText("");
		lblIngreseSuNombre.setBackground(new Color(244, 164, 96));
		lblIngreseSuNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngreseSuNombre.setForeground(new Color(255, 255, 224));
		lblIngreseSuNombre.setFont(new Font("Franklin Gothic Demi", Font.BOLD, 15));
		lblIngreseSuNombre.setBounds(517, 207, 170, 32);
		frame.getContentPane().add(lblIngreseSuNombre);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Menu.class.getResource("/imagenes/menu.jpg")));
		// lblNewLabel.setIcon(new ImageIcon(Menu.class.getResource("menu.jpg")));
		lblNewLabel.setBounds(0, 0, 800, 600);
		frame.getContentPane().add(lblNewLabel);

	}

}
