package vista;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import intermediario.Juego;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Color;

import java.awt.Toolkit;

public class Preguntas extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private Juego juego;
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Preguntas dialog = new Preguntas();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public Preguntas() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Preguntas.class.getResource("/imagenes/pregunta.png")));
		setTitle("Pregunta");
		setBackground(new Color(184, 134, 11));
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			juego = new Juego();

			lblNewLabel = new JLabel(juego.pregunta());
			lblNewLabel.setBackground(new Color(189, 183, 107));
			lblNewLabel.setForeground(new Color(0, 0, 0));
			lblNewLabel.setFont(new Font("Elephant", Font.BOLD, 15));
			lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel.setBounds(44, 41, 345, 146);

			contentPanel.add(lblNewLabel);
		}

		JButton btnNewButton = new JButton("SI");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				juego.escucharRespuesta(e.getActionCommand());
			}
		});
		btnNewButton.setForeground(new Color(238, 232, 170));
		btnNewButton.setBackground(new Color(128, 128, 128));
		btnNewButton.setBounds(38, 209, 89, 23);
		contentPanel.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("NO");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				juego.escucharRespuesta(e.getActionCommand());
			}
		});
		btnNewButton_1.setBackground(new Color(128, 128, 128));
		btnNewButton_1.setForeground(new Color(238, 232, 170));
		btnNewButton_1.setBounds(157, 209, 89, 23);
		contentPanel.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("SIGUIENTE");
		btnNewButton_2.setForeground(new Color(238, 232, 170));
		btnNewButton_2.setBackground(new Color(128, 128, 128));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				juego.escucharRespuesta(e.getActionCommand());
			}
		});
		btnNewButton_2.setBounds(294, 209, 111, 23);
		contentPanel.add(btnNewButton_2);

		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(Preguntas.class.getResource("/imagenes/fondoPregunta.jpg")));
		label_1.setBounds(44, 41, 345, 146);
		contentPanel.add(label_1);

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Preguntas.class.getResource("/imagenes/fondoJuego.jpg")));
		label.setBounds(-128, 0, 562, 261);
		contentPanel.add(label);

	}
}
