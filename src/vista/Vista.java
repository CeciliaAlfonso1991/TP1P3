package vista;

import java.awt.EventQueue;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import intermediario.Juego;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ButtonGroup;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import javax.swing.border.CompoundBorder;
import java.awt.Color;

public class Vista {

	private JFrame frame;
	private Preguntas pregunta;
	private Juego juego;
	private String nombre;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vista window = new Vista();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Vista() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		juego = new Juego();
		frame.setBounds(100, 100, 965, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		pregunta = new Preguntas();
		nombre = juego.nombreIngresado();

		JButton btnPregunta = new JButton("Pregunta");
		btnPregunta.setForeground(new Color(238, 232, 170));
		btnPregunta.setToolTipText("");
		btnPregunta.setBackground(new Color(160, 82, 45));
		btnPregunta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pregunta.setVisible(true);

			}
		});

		JLabel lblSeleccioneElContinente = new JLabel(
				"PIENSE UN PA\u00CDS, LUEGO SELECCIONE EL CONTINENTE AL QUE PERTENECE.");
		lblSeleccioneElContinente.setForeground(new Color(128, 0, 0));
		lblSeleccioneElContinente.setBounds(108, 85, 732, 53);
		frame.getContentPane().add(lblSeleccioneElContinente);
		lblSeleccioneElContinente.setBackground(new Color(210, 180, 140));
		lblSeleccioneElContinente.setLabelFor(frame);
		lblSeleccioneElContinente.setToolTipText("");
		lblSeleccioneElContinente.setHorizontalAlignment(SwingConstants.CENTER);
		lblSeleccioneElContinente.setFont(new Font("Elephant", Font.BOLD, 10));
		btnPregunta.setBounds(108, 503, 132, 23);
		frame.getContentPane().add(btnPregunta);

		JButton btnIngresarPas = new JButton("Ingresar pa\u00EDs");
		btnIngresarPas.setForeground(new Color(238, 232, 170));
		btnIngresarPas.setBackground(new Color(160, 82, 45));
		btnIngresarPas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				juego.ingresarPaisPensado();
			}
		});
		btnIngresarPas.setBounds(250, 503, 132, 23);
		frame.getContentPane().add(btnIngresarPas);

		JButton btnPuntajes = new JButton("Puntajes");
		btnPuntajes.setForeground(new Color(238, 232, 170));
		btnPuntajes.setBackground(new Color(160, 82, 45));
		btnPuntajes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				juego.mostrarPuntajes();
			}
		});
		btnPuntajes.setBounds(392, 503, 132, 23);
		frame.getContentPane().add(btnPuntajes);

		JButton btnSalir = new JButton("Salir");
		btnSalir.setForeground(new Color(238, 232, 170));
		btnSalir.setBackground(new Color(160, 82, 45));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		btnSalir.setBounds(708, 503, 132, 23);
		frame.getContentPane().add(btnSalir);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(128, 128, 128));
		panel.setBorder(new CompoundBorder());
		panel.setBounds(108, 141, 732, 33);
		frame.getContentPane().add(panel);

		JRadioButton rdbtnOceana = new JRadioButton("Ocean\u00EDa");
		rdbtnOceana.setBackground(new Color(128, 128, 128));
		panel.add(rdbtnOceana);
		buttonGroup.add(rdbtnOceana);

		JRadioButton rdbtnAsia = new JRadioButton("Asia");
		rdbtnAsia.setBackground(new Color(128, 128, 128));
		panel.add(rdbtnAsia);
		buttonGroup.add(rdbtnAsia);

		JRadioButton rdbtnAmrica = new JRadioButton("Am\u00E9rica");
		rdbtnAmrica.setBackground(new Color(128, 128, 128));
		panel.add(rdbtnAmrica);
		buttonGroup.add(rdbtnAmrica);

		JRadioButton rdbtnAfrica = new JRadioButton("Africa");
		rdbtnAfrica.setBackground(new Color(128, 128, 128));
		panel.add(rdbtnAfrica);
		buttonGroup.add(rdbtnAfrica);

		JRadioButton rdbtnEuropa = new JRadioButton("Europa");
		rdbtnEuropa.setBackground(new Color(128, 128, 128));
		panel.add(rdbtnEuropa);
		buttonGroup.add(rdbtnEuropa);
		rdbtnEuropa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				juego.filtrarPaises(e.getActionCommand());
			}
		});
		rdbtnAfrica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				juego.filtrarPaises(e.getActionCommand());
			}
		});
		rdbtnAmrica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				juego.filtrarPaises(e.getActionCommand());
			}
		});
		rdbtnAsia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				juego.filtrarPaises(e.getActionCommand());
			}
		});
		rdbtnOceana.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				juego.filtrarPaises(e.getActionCommand());
			}
		});

		JLabel lblNombre = new JLabel("NOMBRE: " + nombre);
		lblNombre.setFont(new Font("Elephant", Font.PLAIN, 12));
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombre.setBounds(108, 47, 732, 41);
		frame.getContentPane().add(lblNombre);

		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBackground(new Color(165, 42, 42));
		lblNewLabel_1.setIcon(new ImageIcon(Vista.class.getResource("/imagenes/mapa.jpg")));
		lblNewLabel_1.setBounds(108, 185, 734, 311);
		frame.getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(-39, -46, 1063, 814);
		frame.getContentPane().add(lblNewLabel);
		lblNewLabel.setIcon(new ImageIcon(Vista.class.getResource("/imagenes/fondoJuego.jpg")));

	}

	public Window vistaFrame() {
		// TODO Auto-generated method stub
		return frame;
	}
}
